

import { Template } from 'meteor/templating';
import { Tasks } from '../api/tasks.js';
import './task.html';

Template.task.events({
    // listen toggle-checked class on click
    'click .toggle-checked' () {
        // Set the checked property to the opposite of its current value
        Tasks.update(this._id, {
            $set: { checked: !this.checked },
        });
    },

    // listen delete class on click
    'click .delete' () {
        Tasks.remove(this._id);
    },
});

