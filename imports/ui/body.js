// 2. création du body.js qui va nourrir mes variables grace au 'helpers'.
// body.helpers (concerne seulement le body)
import { Template } from 'meteor/templating';
import './body.html';

// Template.body.helpers({
//     tasks: [
//         { text: 'This is task 1' },
//         { text: 'This is task 2' },
//         { text: 'This is task 3' },
//     ],
// });

// 5. remplace le tableau précédent qui était statique
import { Tasks } from '../api/tasks.js';

Template.body.helpers({
    tasks () {
        // Show newest tasks at the top
        return Tasks.find({}, { sort: { createdAt: -1 } });
    },
});

Template.body.events({
    // on ecoute l'event submit sur tout ce qui matche avec '.new-task', cad le formulaire
    'submit .new-task' (event) {
        event.preventDefault();

        // Get value from form element
        const target = event.target;
        const text = target.text.value;

        // Insert a task into the collection
        Tasks.insert({
            text,
            createdAt: new Date(), // current time
        });
        // Clear form to type another task
        target.text.value = '';
    },
});


